#!/usr/bin/python3

################################################################################
##  Project: avg3                                                             ##
##           script to read in three arguments on the command line and        ##
##           finds their average to two decimal places.                       ##
##  Cintia Celi                                                               ##
##                                                                            ##
################################################################################

'''ski-avg3 3 5 9
and it will print out:
Average of 3, 5, and 9 is 5.67

If I enter something like:
* ski-avg3 3 5
* ski-avg3 3 4 5 6
* ski-avg3 3 one 5
it needs to print an error telling me how to use it.'''

import sys

def avg(arguments): 
    mysum = 0
    for arg in arguments:
        mysum += arg
    return (mysum/len(arguments))
    
def main():
    try:
        arg1, arg2, arg3 = map(float, (sys.argv[1:])) # input("cintia-avg3: \t").split())
        arguments = [arg1, arg2, arg3]
        average = avg(arguments)
        print("Average of %s, %s, and %s is %s" %(arg1, arg2, arg3, round(average,2)))
    except ValueError as e:
        print("ValueError:", e )
        
if __name__ == "__main__":
    main()
