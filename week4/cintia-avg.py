#!/usr/bin/python3

################################################################################
##  Project: avg - extra credit excercise                                     ##
##           script to read arguments on the command line and                 ##
##           finds their average to two decimal places.                       ##
##  Cintia Celi                                                               ##
##                                                                            ##
################################################################################

'''Extend the avg script to work w/any number of numbers from 1 to n.
It should error with a how to use message if there are no arguments.
Use functions(module/an interactive instance) so if I import it into the interpreter,
I can call the function in the script like: //so a ommand line argument

>>> import ./ski-avg
>>> ski-avg.avg(3, 5, 9)
>>> 5.67
or I can run the script as a main program like ski-avg 3 5 9 11 8 45'''

import sys
from statistics import mean

#parameter to return the list of command line arguments that will be passed to this program when run
        
def avg(numbers):
    try:
        print(mean(map(float, numbers)))
    except ValueError as e:
        print("ValueError: ", e )
        sys.exit()
    except TypeError as e:
        print("TypeError: ", e )
        sys.exit()
    
def main():
    if len (sys.argv) >= 2:
        numbers = (sys.argv[1:])
        average = avg(numbers)
        
    else:
        #filename = sys.argv[0]
        function = sys.argv[1]
        numbers = sys.argv[2:]
        average = fuction(numbers)
        
if __name__ == "__main__":
    main()
