#!/usr/bin/python3
################################################################################
#                                 Cintia Celi                                ###
#                                     Log-4                                  ###
#    Script to produce csv file listing mac addresses, ips & number of acks  ###
################################################################################
# machine program is running on: ThinkPad
'''This script will search for an IP in the dhcpdsmall.log file, get the mac address
and then lookup the mac address at https://api.macvendors.com/ to print out the vendor
if the network device.  Please see https://macvendors.com/api (Links to an external site.)
for how to use the API.  The output should be a csv file.  The input file has these IPs.
10.172.219.117
10.192.125.251
10.172.219.26
10.191.155.13
The output csv file should have:
IP, Mac Address, Vendor'''

import re, csv, sys
import subprocess as sp
from urllib import request, parse
from urllib.parse import urlencode, quote_plus
import logging, threading

ips = {}
params = {}
event = threading.Event()

'''contex manager to open the file'''
with open('dhcpdsmall.log', 'r') as file_in:
    file_reader = file_in.readlines()
    for line in file_reader:
        match = re.search('DHCP.* (\w*) (.*) \(*\w* (([0-9a-fA-F][0-9a-fA-F][:]){5}([0-9a-fA-F]{2})) (.*)', line)
        if match:
            ip = match.group(2)
            mac = match.group(3)                                                                             if (ip in ips) and (mac == ips[ip]['mac']):
                continue
            else:
                params = {'mac': mac}
                querystring = parse.urlencode(params)
                url = "https://api.macvendors.com/" + "?" + querystring
                event.wait(2)
                resp = request.urlopen(url)
                html = resp.read().decode("utf-8")
                ips[ip] = {"mac":mac, "vendor":html}

'''context manager to write to csv file'''
with open('log4.csv', 'w', newline = '') as fout:
    csv_writer = csv.writer(fout)
    fieldnames = ['    IP    ', '  Mac address  ', '  Vendor ']
    csv_writer = csv.writer(fout, delimiter=',')
    csv_writer.writerow(fieldnames)
    for ip in ips:
        row = [ip, (ips[ip]['mac']), (ips[ip]['vendor'])]
        csv_writer.writerow(row)

                
