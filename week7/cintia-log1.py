#!/usr/bin/python3
################################################################################
################################################################################
#                                 Cintia Celi                                ###
#                                     Log-1                                  ###
#    Script to read dhcpd log and print out all mac addresses that belong    ###
#    that are iphones.  Besides keeping a count of each iPhone               ###
################################################################################
################################################################################
# machine program is running on: ThinkPad
'''Script to read the dhcpd log and print all mac addresses that are iphones.
Also print out a count of the total number of unique iphone mac addresses.
So there is a file with only the iPhone mac addresses listed and at the bottom,
a count of the total number of unique addresses.  Run this against
the dhcpdsmall.log file (4200 lines).
Output should look like this:

macaddress1

macaddress2

Count = 2
(in this example there were two phones).  When script is run again dhcpdsmall
there will be more than 2 phones.
Extra credit: Run this against the dhcpd.log file for an extra 10 points.
How long did it take? '''

import re, csv, sys

'''contex manager to open the file'''
with open('dhcpdsmall.log', 'r') as file_in:
    macs = []
    file_reader = file_in.readlines()  #to read the text file using a dialog,

    for line in file_reader:
       # match = re.search('([a-f0-9]{2}(:)){5}[a-f0-9]{2} [(\w-]*iPhone', line)
        match = re.search('DHCPACK on .*to (.*) .*iPhone', line)
        if match:
            mac = match.group(1)

            if mac in macs:
                continue
            else:
                macs.append(mac)
   '''printing output'''
    print(*macs, sep = "\n")
    print("\nCount = ", len(macs))

'''contex manager to write the file'''
with open('log1.txt', 'w', newline = '') as fout:
    csv_writer = csv.writer(fout)
    for mac in macs:
        fout.write('%s\n' % mac)
