#!/usr/bin/python3
################################################################################
################################################################################
#                                 Cintia Celi                                ###
#                                     Log-2                                  ###
#    Script to print dhcpd log conversation for passed mac address           ###
################################################################################
################################################################################
# machine program is running on: ThinkPad

'''Read the dhcpdsmall log and print out the entire DHCP conversation for mac addresses
are passed in on the command line.  So you can run your script with a mac address on
the command line and it will list all the dhcp traffic associated with that mac address.
The output is just the lines in the file in the conversation.  Test with:
00:10:49:1b:d0:36

84:8e:0c:1d:58:60

9c:b7:0d:8a:29:65

using the dhcpdsmall.log file.
Extra 10 points if you can explain to me in an email what the different results mean on
each of the above mac addresses.'''

import re, csv, sys

'''code to receive mac address and check input'''
if sys.argv[1:]:
    mac = sys.argv[1]
else:
    print("Enter a mac address")

'''contex manager to open the file'''
with open('dhcpdsmall.log', 'r') as file_in:
    file_reader = file_in.readlines()
    for line in file_reader:
        if mac in line:
            print(line)
