#!/usr/bin/python3
####################################################################
#                           Cintia Celi                          ###
#                             Log-5                              ###
####################################################################

import os, re, sys, csv

matches = {}
tra_reg={"date=": "(\d{4}(-\d\d){2})", "time=": "((\d\d:){2}\d\d)", "app=":"(\w*)", "service=":"(\w*)","srcip=": "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})){3})", "dstip=": "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})){3})","dstcountry=": "(\"\"\w*)","hostname=": "(([0-9a-fA-F][0-9a-fA-F][:]){5}([0-9a-fA-F]{2}))", "policyid=":"(\d{3})", "sessionid=":"(\d{10})"}
web_reg={"group=": "(\w*)", "dstip=": "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})){3})", "url=": "(http|ftp|https)(:\/\/)([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])", "referralurl=": "(http|ftp|https)(:\/\/)([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])", "action=": "(\w*)", "eventtype=":"(\w*)", "cat=": "(\w*)", "catdesc=": "(\w*)", "initiator=":"(\w*)", "keyword=":"(\w*)", "msg=": "(\"\"(.)*([^b]\"\"\"))", "profile=":"((\w*)(.)*Profile)"}

def logs5(logs, regx):
    with open('output.csv', 'w', newline='') as fout:
            writer = csv.writer(fout, delimiter=',')
            writer.writerow([key for key in regx])
            
    with open(logs, 'r') as file_in:
        reader = file_in.readlines()
        for line in reader:
            for key, value in regx.items():
                match = re.search(key+value, line)
                if match:
                    matches[key]=match.group(1)
                else:
                    matches[key]=""
                                        
                    with open('output.csv', 'a', newline='') as fout:
                        writer = csv.writer(fout, delimiter=',')
                        writer.writerow([value for value in matches.values()])
def main():
    if sys.argv[1:]:
        reg = sys.argv[1]
        logs = sys.argv[2]

        if reg == "traffic":
            regx = tra_reg
        else:
            regx = {**tra_reg, **web_reg}  #Everything that is in the traffic log PLUS:
        logs5(logs, regx)
            
    else:
        print("Pass a log file:\t")

if __name__ == "__main__":
    main()
                                                
