#!/usr/bin/python3
################################################################################
#                                 Cintia Celi                                ###
#                                     Log-3                                  ###
#    Script to produce csv file listing mac addresses, ips & number of acks  ###    
################################################################################
# machine program is running on: ThinkPad

'''Script to read the dhcpdsmall.log file and produce a csv file that has a listing of:
Mac address, IP address, Total number of acks
For each Mac address it finds in the dhcpdsmall.log file.
Email me the two mac addresses that are having problems (high number of Acks) when you
submit your script via git.

Hint: use MacAddress-IP as the key to your dictionary/hash to keep track of how often
you see it the MacAddress on that IP'''

import re, csv, sys
from csv import DictWriter
ips = {}

'''contex manager to open the file'''
with open('dhcpdsmall.log', 'r') as file_in:
    file_reader = file_in.readlines()
    for line in file_reader:
        match = re.search('DHCPACK on (.*) to (.*) via', line)
        if match:
            ip = match.group(1)
            mac = match.group(2)

            if (ip in ips) and (mac == ips[ip]['mac']):
                counter = ips[ip]['counter']
                counter += 1
                ips[ip]['counter'] = counter
            else:
                ips[ip] = {"mac":mac, "counter":1}                                               

'''context manager to write to csv file'''
with open('log3.csv', 'w', newline = '') as fout:
    csv_writer = csv.writer(fout)
    fieldnames = ['Mac address', ' IP address', 'Total number of ACKs']
    csv_writer = csv.writer(fout, delimiter=',')
    csv_writer.writerow(fieldnames)
    for ip in ips:
        row = [(ips[ip]['mac']), ip, (ips[ip]['counter'])]
        csv_writer.writerow(row)
