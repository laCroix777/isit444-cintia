#!/usr/bin/python3
####################################################################
#                           Cintia                               ###
#                            Ping-3                              ###
#  Script to ping IPs listed on a text file or IP form.          ###
####################################################################

# machine program is running on: ThinkPad
'''This script reads either a file(i.e. pingfile.txt), IP or server/computer
IPs and names. Pings each one and prints the IP and the time to ping in the
format shown below. If it cannot pinga a machine it returns "Not Found".
IP, TimeToPing(ms)
178.4.5.6, 8.4
192.168.2.34, Not found.
Also, it checks if the input is a file in the same directory as the script.
If it is not a file then assume it is a DNS name or IP.  If it is a file,
then you have to read the file and ping the items in the file.
So you can run the script like:
> ski-ping3.py pingfile.txt
> ski-ping3.py google.com
> ski-ping3.py 192.168.3.4 '''

import os, re, sys
import subprocess as sp

'''function to Read file, extract information, ping the ip and print'''
def ping3(line):
    try:
        res = sp.check_output(["ping", "-c", "1", line], shell=False, stderr=sp.STDOUT).decode('utf8')

        '''to select the groups identifiers'''
        match = re.search('time=(.*)ms', res)
        if match:
            time = match.group(1)
        else:
            time = "Not Found"

        '''printing results'''
        print(line + ', ' + time)

    except:
        print(line + ", Not Found")

def main():
    if sys.argv[1:]:
        args = sys.argv[1]

        if os.path.exists(args):
            print("    IP,   TimeToPing(ms)")
            file_in = open(args, 'r')
            '''loop over each line and ping the ip'''
            for line in file_in:
                line = line.rstrip()
                ping3(line)
        else:
            print("    IP    TimeToPing(ms)")
            ping3(args)
    else:
        print("Enter a File or Server IP as argument")
     
if __name__ == "__main__":
    main()
