#!/usr/bin/python3
####################################################################
#                           Cintia                               ###
#                            Ping-2                              ###
#  Script to ping IPs or addresses passed via the cmd-line.      ###
####################################################################

# machine program is running on: ThinkPad 
'''This script reads users cmd-line input of IP or hostname, pings it, and
displays the result in format shown below: 
IP, TimeToPing(ms)
google.com, 15.6
If an unknown domain or IP are entered, output would be as:
IP, TimeToPing(ms)
192.168.34.34, Not Found.
The script will be called as follows'''

import os, re, sys
import subprocess as sp
      
'''This script reads users cmd-line input of IP or hostname, pings it, and
then displays the result in the same format on the screen. Eg: > ski-ping2.py google.com
and print out something like:
IP, TimeToPing(ms)
google.com, 15.6

or if you enter an unknown domain or IP you should get:
IP, TimeToPing(ms)
192.168.34.34, Not Found.'''

import sys, os, re
import subprocess as sp

'''function to read argument passed, ping, and print results'''
def ping2(args):
    try:
        '''pring the ip or hostname passed as argument'''
        res = sp.check_output(['ping','-c', '1',  args], shell=False, stderr=sp.STDOUT).decode('utf8')
        '''evaluate response and print'''
        match = re.search("time=(.*)ms", res)
        if match:
            time = match.group(1)
        else:
            time = "Not Found"

        '''printing results'''
        print("    IP,   TimeToPing(ms)")
        print(args + ", " + time)
    except:
        print(args + ', Not Found')

def main():
    if sys.argv[1:]:
        args = sys.argv[1]
        ping2(args)
    else:
        print("Enter an IP or hostname as argument")

if __name__ == "__main__":
    main()
                                                
