#!/usr/bin/python3
####################################################################
#                           Cintia                               ###
#                            Ping-1                              ###
#  Script to ping IPs listed on a text file an record response.  ###
####################################################################

# machine program is running on: ThinkPad
'''This script reads a file(pingfile.txt) of computer IPs and names.
Pings each one and prints the IP and the time to ping in the format
shown below. If it cannot ping a machine it returns "Not Found".
IP, TimeToPing(ms)
10.1.2.3, 10
google.com, 5
192.168.2.3, NotFound
Script runs from the command line taking the file as the command line argument
like so> ski-ping1.py pingfile.txt. Add the script to the pingfile.txt to git repo.'''

import os, re, sys
import subprocess as sp

'''method to Read file, extract information, ping the ip and print'''
def ping1(line):
    try:
        '''ping the ip'''
        res = sp.check_output(["ping", "-c", "1", line], shell=False, stderr=sp.STDOUT).decode("utf8")
        '''to select the groups identifiers'''
        match = re.search('time=(.*)ms', res)
        if match:
            time = match.group(1)
        else:
            time = "Not Found"
        '''printing results'''
        print(line + ", " + time)

    except:
        print(line + ", Not Found")

def main():
    if sys.argv[1:]:
        args = sys.argv[1]

        if os.path.exists(args):
            print("    IP,     TimeToPing(ms)")
            file_in = open(args, 'r')
            '''looping over the file'''
            for line in file_in:
                line=line.rstrip()
                ping1(line)
        else:
            print("File Not Found or invalid")

    else:
        print("Enter a file as input argument")

if __name__ == "__main__":
    main()
