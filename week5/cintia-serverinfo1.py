#!/usr/bin/python3

#pip install get-nic
#############################################################################
#                            Cintia                                       ###
#                            ServerInfo1                                  ###
#  Script to display Hostname, CPU, RAM, OS V, Disk Cnt, NIC name + Cnt.  ###              ###
#############################################################################

''' Code to be called as: > ski-serverinfo1.py
and Display the following information:
HostName = Mido
CPU (count) = 4
RAM (GB) = 8
OSType = Windows
OSVersion = 10
Disks (Count) = 1
NIC (Count) = 3
NIC Names/IPs: 
  Ethernet0 = 10.5.0.30
  Bluetooth Network Connection = 169.254.190.239
  Loopback Pseudo-Interface 1 = 127.0.0.1 '''
  
# Importing libraries
import socket as so
import platform as ptf
import re, uuid, json, psutil, string, shutil #, netifaces
from get_nic import getnic
  
# Function to display server information listed above
def getSystemInfo():
    try:
        info = {}
        info["HostName"] = so.gethostname()
        info["CPU (count)"] = psutil.cpu_count(logical=False)
        info["RAM (GB)"] = str(round(psutil.virtual_memory().total / (1024.0 **3)))+" GB"
        info["OS Type"] = ptf.system()
        info["platform-version"] = ptf.version()
        info["Disk (count)"] = (len(shutil.disk_usage(path="C:/")) - 1)
        info["NIC (count)"] = psutil.NIC_DUPLEX_HALF + psutil.NIC_DUPLEX_FULL
        
        #info["NIC Names/IPs"] = [getnic.interfaces(), getnic.ipaddr(getnic.interfaces())]
        #info["NIC Names/IPs"] = [so.gethostname(), so.gethostbyname(so.gethostname())]

        return json.dumps(info)
    except Exception as e:
        logging.exception(e)

print(json.loads(getSystemInfo()))
  
### Driver code 
##get_Host_name_IP() #Function call 


#info["Disk (count)"] = len(psutil.disk_usage(path="C:/")) #/proc/mounts
#info["Disk (COUNT3"] = windll.kernel32.GetLogicalDrives()
#addrs = psutil.net_if_addrs()
#print("NIC Names/IPs = ", addrs.keys())
#int_list = netifaces.interfaces()
#info["NIC Names/IPs"] = so.if_nameindex()
#info["NIC Names/IPs"] = (filter(lambda x: 'io' in x, netifaces.interfaces()))
#info["NIC Ethernet0"] = so.if_nameindix(socket.gethostbyname()
#info["ip-address "] = so.gethostbyname(so.gethostname())
