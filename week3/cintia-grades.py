#!/usr/bin/python3

####################################
###        Cintia Celi          ####
###     Project Grades          ####
####################################

# Grade Calculator90 - A program that will average 3 numeric exam grades,
# return an average test score, a corresponding letter grade, and a message
# stating whether the student is passing.

# 90+ = A, 80 - 89 =B, 70 - 79 = C 60 - 69 = D, < 59 = F

def avg(grades):
    # Get average grade 
    mysum = 0   #changed sum with mysum as sum is a python function.
    for grade in grades:
        mysum += grade #corrected name to grade
    return (mysum/len(grades))
    
def grade2letter(gavg): #was calling the function avg, so change input to "gavg"
    # Convert grade to letter grade
    
    if ((gavg >= 90) and (gavg <= 100)):
        letter_grade = "A"
    elif ((gavg >= 80) and (gavg < 90)):
        letter_grade = "B"
    elif ((gavg > 69) and (gavg < 80)):
        letter_grade = 'C'
    elif ((gavg >= 60) and (gavg <= 69)):  #added closing column and changed 65/69, changed 65 with 60, and the signs
        letter_grade = "D"
    else:                       #changed elif with else
        letter_grade = "F"

    return letter_grade

# Print out the grade
def print_grade(grades, gavg, letter_grade): #added comma to separate input statements, added avg as input and changed to grades
    print(" ") # changed starting single quote with double quote
    for grade in grades:
        print("Exam: ", grade)
    print("Average: ", round(gavg)) #Round 2 decimal places?! round(gavg,2)
    print("Grade: " + str(letter_grade))

    if letter_grade is "F":
        print("Student is failing.")
    else:
        print("Student is passing.")

def main():
    # Get Data from user and put into an array
    exam_one = float(input("Input exam grade one: ")) #was missing ending "
    exam_two = float(input('nput exam grade two: ')) #missing closing ' and int instead of string
    exam_three = float(input("Input exam grade three: ")) #changed name from 3 to three, added int and closing "
    grades = [exam_one, exam_two, exam_three]  #added commas to separate items, changed exam_2 to two, changed the curly bracket 

    # Call functions to do the work and print output
    gavg = avg(grades) #remove 1 as the function is only "avg" and changed grade to grades
    letter_grade = grade2letter(gavg)
    print_grade(grades, gavg, letter_grade) #(print+grade was incorrect)

# Run main() if script called directly
if __name__ == "__main__":
        main()
