#!/usr/bin/python3
####################################################################
#                           Cintia Celi                          ###
#                            users2                              ###
#  Script to add 4 variable identify information to csv file     ###
####################################################################
# machine program is running on: ThinkPad
'''This script is the same as user1, but creates a new field called userid that is
 the first initial+lastname+N for staff.  N is a number from 2 - 9 that is added
to a userid if that userid already exists. The script keeps track of all userids
previously created. A dictionary has a field to add a number to a userid. It then
writes out a line to a file with all the information including the staff userid.
So for the staff account for chris kacoroski the userid would be ckacoroski.  LIke
user1, the script loop so you can add more than one line and have a way for the
 user to tell the script when they are done entering accounts.
The script runs like so: > ski-user2.py

It will loop asking for both student and staff accounts until the user says done.
The resulting user2.csv file will look something like (test with your own made up users):

firstname, lastname, school, accounttype, userid
Ski, Kacoroski, NJH, staff, skacoroski
Joy, Kacoroski, NJH, student,
Steve, Kacoroski, AH, staff, skacoroski2

I will check that you do NOT add 1 to the end of a userid (must start
with 2) and that you do not skip a number when adding it to a userid
(e.g. skacoroski then skacoroski2, then jkacoroski, then not jkacoroski3
(must be jkacoroski2).'''

import re, os, sys, csv

schools = ['IHS', 'NJH', 'AH', 'BHS', 'BC']
accounts = ['staff', 'student']
users = {}

'''function to add user to the csv file'''
def users2(first, last, sch, acc, csv_writer):
    global users, userid

    if acc == 'staff':
        userid = userID(first, last)
        users[userid] = 1
        return [first, last, sch, acc, userid]

    else:
        return [first, last, sch, acc, userid]

'''creates a new field named userid = first initial+lastname+N for staff.
 N is a number from 2 - 9. Added to a userid if that userid already exists. '''
def userID(first, last):
    y = 2
    ini = first[0]
    basename = ini+last
    uid =  basename

    while uid in users:
        uid = basename + str(y)
        y += 1
    return uid

def main():
    '''context manager to write header'''
    with open ('users2.csv', 'w', newline = '') as fout:
        fieldnames = ['FirstName', ' LastName', ' School', ' AccountType']
        csv_writer = csv.writer(fout, delimiter=',')
        csv_writer.writerow(fieldnames)

        first, last, sch, acc = input("Enter: name last_name school account_type:\t ").split()
        while True:
            if sch not in schools:
                print("School does not match existing school list:\t", schools)
            elif acc not in accounts:
                print("Account does not match existing account list:\t", accounts)
            else:
                user_data = users2(first, last, sch, acc, csv_writer)
                csv_writer.writerow(user_data)

            choice = input("Do you want to continue? (y/n):  ")
            if choice != "y":
                break
            else:
                first, last, sch, acc = input("Enter: name last_name school account_type:  ").split()

if __name__ == "__main__":
    main()
    
