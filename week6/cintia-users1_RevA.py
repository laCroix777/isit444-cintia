#!/usr/bin/python3
####################################################################
#                           Cintia Celi                          ###
#                            users1. Revision A                  ###
#  Script to add 4 variable identify information to csv file     ###
####################################################################
# machine program is running on: ThinkPad
'''This script prompts for firstname, lastname, school, and account type.  It then
writes a header line and a single data line to the users1.csv
output file for each input with this information.  School must be one of IHS, NJH,
AH, BHS, or BC (any other value is not allowed and it must be in CAPS).
Account type can be staff or student (any other value is not allowed and it must
be lowercase).  Scrip has error checking so only valid values work.  If user enters
a non-valid value, script display the allowed values and loop back around until
valid values are entered.  Also the user exit the script if they change their mind.
Scrip loops asking for both student and staff accounts until user says they are done.
Run the script like:  > ski-user1.py
The output file lines that looks like:
firstname, lastname, school, accounttype
Ski, Kacoroski, IHS, staff
Joy, Smith, NJH, student'''

import re, os, sys, csv

schools = ['IHS', 'NJH', 'AH', 'BHS', 'BC']
accounts = ['staff', 'student']
users = {}

'''function to add user to the csv file'''
def users1(first, last, sch, acc):
    users = {"first":first, "last":last, "sch":sch, "acc":acc}
    return [first, last, sch, acc]                                                              
def main():
    global csv_writer
    '''context manager to write the file'''
    with open ('users1.csv', 'w', newline = '') as fout:
        fieldnames = ['FirstName', ' LastName', ' School', ' AccountType ']
        csv_writer = csv.writer(fout, delimiter=',')
        csv_writer.writerow(fieldnames)

        '''request input from user'''
        first, last, sch, acc = input("Enter: name last_name school account_type:\t ").split()

        '''verifying input validity'''
        while True:
            if sch not in schools:
                print("School does not match existing school list:\t", schools)
            elif acc not in accounts:
                print("Account does not match existing account list:\t", accounts)
            else:
                user_object = users1(first, last, sch, acc)
                csv_writer.writerow(user_object)
            choice = input("Do you want to continue? (y/n):  ")
            if choice != "y":
                break
            else:
                first, last, sch, acc = input("Enter: name last_name school account_type:  ").split()

if __name__ == "__main__":
    main()
